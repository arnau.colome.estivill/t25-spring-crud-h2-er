DROP TABLE IF EXISTS `cajas`;
DROP table IF EXISTS `almacen`;

CREATE TABLE `almacen` (
  `id` int NOT NULL AUTO_INCREMENT,
  `lugar` varchar(100) DEFAULT NULL,
  `capacidad` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `cajas` (
  `id` char(5) NOT NULL PRIMARY KEY,
  `contenido` varchar(100) DEFAULT NULL, 
  `valor` int DEFAULT NULL, 
  `id_almacen` int DEFAULT NULL, 
  FOREIGN KEY (`id_almacen`) REFERENCES `almacen` (`id`)
);

insert into almacen (lugar, capacidad) values ('Reus', '2400'), ('Tarragona', '1532'), ('Valls', '3886'), ('Riudoms', '884'), ('Barcelona', '5112');

insert into cajas (id, contenido, valor, id_almacen) values ('22135', 'Material Jardín', '251233', 1), ('22514', 'Alimentos', '133200', 2), ('66321', 'Material Construccion', '622112', 3), ('44155', 'Deportes', '122003', 4), ('11245', 'Material Oficina', '9223', 5), ('55238', 'Plantas', '120332', 1), ('45128', 'Herramientas', '23462', 3);


