package com.crud.h2.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.crud.h2.dto.Almacen;
import com.crud.h2.service.AlmacenServiceImpl;

@RestController
@RequestMapping("/api")
public class AlmacenController {
	
	@Autowired
	AlmacenServiceImpl almacenServideImpl;
	
	@GetMapping("/almacens")
	public List<Almacen> listarAlmacens(){
		return almacenServideImpl.listarAlmacens();
	}
	
	@PostMapping("/almacens")
	public Almacen salvarAlmacen(@RequestBody Almacen almacen) {
		
		return almacenServideImpl.guardarAlmacen(almacen);
	}
	
	@GetMapping("/almacens/{id}")
	public Almacen almacenXID(@PathVariable(name="id") Long id) {
		
		Almacen almacen_xid= new Almacen();
		
		almacen_xid=almacenServideImpl.almacenXID(id);
		
		System.out.println("Almacen XID: "+almacen_xid);
		
		return almacen_xid;
	}
	
	@PutMapping("/almacens/{id}")
	public Almacen actualizarAlmacen(@PathVariable(name="id")Long id,@RequestBody Almacen almacen) {
		
		Almacen almacen_seleccionado= new Almacen();
		Almacen almacen_actualizado= new Almacen();
		
		almacen_seleccionado= almacenServideImpl.almacenXID(id);
		
		almacen_seleccionado.setLugar(almacen.getLugar());
		almacen_seleccionado.setCapacidad(almacen.getCapacidad());
		
		almacen_actualizado = almacenServideImpl.actualizarAlmacen(almacen_seleccionado);
		
		System.out.println("El almacen actualizado es: "+ almacen_actualizado);
		
		return almacen_actualizado;
	}
	
	@DeleteMapping("/almacens/{id}")
	public void eleiminarAlmacen(@PathVariable(name="id")Long id) {
		almacenServideImpl.eliminarAlmacen(id);
	}
	
	
}
