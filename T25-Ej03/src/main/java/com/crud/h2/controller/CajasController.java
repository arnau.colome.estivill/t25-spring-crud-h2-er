package com.crud.h2.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.crud.h2.dto.Cajas;
import com.crud.h2.service.CajasServiceImpl;

@RestController
@RequestMapping("/api")
public class CajasController {

	@Autowired
	CajasServiceImpl cajaServideImpl;
	
	@GetMapping("/cajas")
	public List<Cajas> listarCajas(){
		return cajaServideImpl.listarCajas();
	}
	
	@PostMapping("/cajas")
	public Cajas salvarCaja(@RequestBody Cajas caja) {
		
		return cajaServideImpl.guardarCaja(caja);
	}
	
	@GetMapping("/cajas/{id}")
	public Cajas cajaXID(@PathVariable(name="id") Long id) {
		
		Cajas caja_xid= new Cajas();
		
		caja_xid=cajaServideImpl.cajaXID(id);
		
		System.out.println("Caja XID: "+caja_xid);
		
		return caja_xid;
	}
	
	@PutMapping("/cajas/{id}")
	public Cajas actualizarCaja(@PathVariable(name="id")Long id,@RequestBody Cajas caja) {
		
		Cajas caja_seleccionado= new Cajas();
		Cajas caja_actualizado= new Cajas();
		
		caja_seleccionado= cajaServideImpl.cajaXID(id);
		
		caja_seleccionado.setContenido(caja.getContenido()); 
		caja_seleccionado.setValor(caja.getValor());
		
		caja_actualizado = cajaServideImpl.actualizarCaja(caja_seleccionado);
		
		System.out.println("El caja actualizado es: "+ caja_actualizado);
		
		return caja_actualizado;
	}
	
	@DeleteMapping("/cajas/{id}")
	public void eliminarCaja(@PathVariable(name="id")Long id) {
		cajaServideImpl.eliminarCaja(id);
	}
	
}
