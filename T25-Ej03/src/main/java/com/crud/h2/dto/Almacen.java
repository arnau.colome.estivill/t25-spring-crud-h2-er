package com.crud.h2.dto;

import java.util.List;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="almacen")//en caso que la tabla sea diferente
public class Almacen  {
 

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) //busca ultimo valor e incrementa desde id final de db
	private Integer id;
	private String lugar;
	private Integer capacidad;

    @OneToMany
    @JoinColumn(name="id")
    private List<Cajas> empleado;
	

	public Almacen() {
	
	}

	/**
	 * @param id
	 * @param lugar
	 * @param capacidad
	 */
	public Almacen(Integer id, String lugar, Integer capacidad) {
		//super();
		this.id = id;
		this.lugar = lugar;
	}

	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the lugar
	 */
	public String getLugar() {
		return lugar;
	}

	/**
	 * @param lugar the lugar to set
	 */
	public void setLugar(String lugar) {
		this.lugar = lugar;
	}
	
	/**
	 * @return the capacidad
	 */
	public Integer getCapacidad() {
		return capacidad;
	}

	/**
	 * @param lugar the capacidad to set
	 */
	public void setCapacidad(Integer capacidad) {
		this.capacidad = capacidad;
	}

	/**
	 * @return the empleado
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Empleado")
	public List<Cajas> getEmpleado() {
		return empleado;
	}

	/**
	 * @param empleado the empleado to set
	 */
	public void setEmpleado(List<Cajas> empleado) {
		this.empleado = empleado;
	}

	//Imprimir datos por consola, se puede quitar
	@Override
	public String toString() {
		return "Almacen [id=" + id + ", lugar=" + lugar + ", capacidad=" + capacidad + "]";
	}

}
