package com.crud.h2.dto;

import javax.persistence.*;

@Entity
@Table(name="cajas")//en caso que la tabla sea diferente
public class Cajas {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
	@Column(name = "dni")//no hace falta si se llama igual
	private String id;
	@Column(name = "contenido")//no hace falta si se llama igual
	private String contenido;
	@Column(name = "valor")//no hace falta si se llama igual
	private String valor;
	
	@ManyToOne
	@JoinColumn(name="id_almacen")
	private Almacen caja;

	public Cajas() {
			
	}

	/**
	 * @param id
	 * @param contenido
	 * @param apellido
	 * @param id_almacen
	 */
	public Cajas(String id, String contenido, String valor, Almacen caja) {
		this.id = id;
		this.contenido = contenido;
		this.valor = valor;
		this.caja = caja;
	}

	/**
	 * @return the dni
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param dni the dni to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the contenido
	 */
	public String getContenido() {
		return contenido;
	}

	/**
	 * @param contenido the contenido to set
	 */
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	/**
	 * @return the precio
	 */
	public String getValor() {
		return valor;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}
	


	/**
	 * @return the caja
	 */
	public Almacen getCaja() {
		return caja;
	}

	/**
	 * @param caja the caja to set
	 */
	public void setCaja(Almacen caja) {
		this.caja = caja;
	}

	//Imprimir datos por consola, se puede quitar
	@Override
	public String toString() {
		return "Caja [dni=" + id + ", contenido=" + contenido + ", valor=" + valor + ", id_almacen=" + caja + "]";
	}
	
}
