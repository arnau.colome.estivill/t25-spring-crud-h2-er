DROP TABLE IF EXISTS `articulos`;
DROP table IF EXISTS `fabricante`;


CREATE TABLE `fabricante` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `articulos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `precio` int DEFAULT NULL,
  `id_fabricante` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`id_fabricante`) REFERENCES `fabricante` (`id`)
);


insert into fabricante (nombre) values ('Jose'), ('Paco'), ('Dani'), ('Jordi'), ('Tere');

insert into articulos (nombre, precio, id_fabricante) values ('Pavo', 2, 1), ('Pollo', 3, 2), ('Curry', 1, 3), ('Queso', 2, 1), ('Pato', 5, 4), ('Huevo', 3, 5), ('Lechuga', 2, 3);


