package com.crud.h2.dto;

import javax.persistence.*;

@Entity
@Table(name="articulos")//en caso que la tabla sea diferente
public class Articulos {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
	private Long id;
	@Column(name = "nombre")//no hace falta si se llama igual
	private String nombre;
	@Column(name = "precio")//no hace falta si se llama igual
	private Integer precio;
	@ManyToOne
	@JoinColumn(name="id_fabricante")
	private Fabricante articulo;

	public Articulos() {
			
	}

	/**
	 * @param id
	 * @param nombre
	 * @param precio
	 * @param articulo
	 */
	public Articulos(Long id, String nombre, Integer precio, Fabricante articulo) {
		this.id = id;
		this.nombre = nombre;
		this.precio = precio;
		this.articulo = articulo;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the precio
	 */
	public Integer getPrecio() {
		return precio;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setPrecio(Integer precio) {
		this.precio = precio;
	}

	/**
	 * @return the articulo
	 */
	public Fabricante getArticulo() {
		return articulo;
	}

	/**
	 * @param articulo the articulo to set
	 */
	public void setArticulo(Fabricante articulo) {
		this.articulo = articulo;
	}

	//Imprimir datos por consola, se puede quitar
	@Override
	public String toString() {
		return "Articulo [id=" + id + ", nombre=" + nombre + ", precio=" + precio + ", articulo=" + articulo + "]";
	}
	
}
