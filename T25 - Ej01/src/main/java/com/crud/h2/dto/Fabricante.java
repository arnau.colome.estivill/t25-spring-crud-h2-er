package com.crud.h2.dto;

import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="fabricante")//en caso que la tabla sea diferente
public class Fabricante  {
 

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) //busca ultimo valor e incrementa desde id final de db
	private Long id;
	private String nombre;

    @OneToMany
    @JoinColumn(name="id")
    private List<Articulos> articulo;
	

	public Fabricante() {
	
	}

	/**
	 * @param id
	 * @param nombre
	 */
	public Fabricante(Long id, String nombre) {
		//super();
		this.id = id;
		this.nombre = nombre;
	}

	
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the articulo
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Articulo")
	public List<Articulos> getArticulo() {
		return articulo;
	}

	/**
	 * @param articulo the articulo to set
	 */
	public void setArticulo(List<Articulos> articulo) {
		this.articulo = articulo;
	}

	//Imprimir datos por consola, se puede quitar
	@Override
	public String toString() {
		return "Fabricante [id=" + id + ", nombre=" + nombre + "]";
	}
	
	
	
	
	
}
