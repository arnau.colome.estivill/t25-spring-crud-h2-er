package com.crud.h2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T25Ej01Application {

	public static void main(String[] args) {
		SpringApplication.run(T25Ej01Application.class, args);
	}

}
