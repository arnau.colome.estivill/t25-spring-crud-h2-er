package com.crud.h2.dto;

import java.util.List;
import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="departamento")//en caso que la tabla sea diferente
public class Departamento  {
 

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) //busca ultimo valor e incrementa desde id final de db
	private Integer id;
	private String nombre;
	private Integer presupuesto;

    @OneToMany
    @JoinColumn(name="id")
    private List<Empleados> empleado;
	

	public Departamento() {
	
	}

	/**
	 * @param id
	 * @param nombre
	 * @param presupuesto
	 */
	public Departamento(Integer id, String nombre, Integer presupuesto) {
		//super();
		this.id = id;
		this.nombre = nombre;
	}

	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * @return the presupuesto
	 */
	public Integer getPresupuesto() {
		return presupuesto;
	}

	/**
	 * @param nombre the presupuesto to set
	 */
	public void setPresupuesto(Integer presupuesto) {
		this.presupuesto = presupuesto;
	}

	/**
	 * @return the empleado
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Empleado")
	public List<Empleados> getEmpleado() {
		return empleado;
	}

	/**
	 * @param empleado the empleado to set
	 */
	public void setEmpleado(List<Empleados> empleado) {
		this.empleado = empleado;
	}

	//Imprimir datos por consola, se puede quitar
	@Override
	public String toString() {
		return "Departamento [id=" + id + ", nombre=" + nombre + ", presupuesto=" + presupuesto + "]";
	}

}
