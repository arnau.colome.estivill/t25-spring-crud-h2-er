package com.crud.h2.dto;

import javax.persistence.*;

@Entity
@Table(name="empleados")//en caso que la tabla sea diferente
public class Empleados {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)//busca ultimo valor e incrementa desde id final de db
	@Column(name = "dni")//no hace falta si se llama igual
	private String id;
	@Column(name = "nombre")//no hace falta si se llama igual
	private String nombre;
	@Column(name = "apellidos")//no hace falta si se llama igual
	private String apellidos;
	
	@ManyToOne
	@JoinColumn(name="id_departamento")
	private Departamento empleado;

	public Empleados() {
			
	}

	/**
	 * @param id
	 * @param nombre
	 * @param apellido
	 * @param id_departamento
	 */
	public Empleados(String id, String nombre, String apellidos, Departamento empleado) {
		this.id = id;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.empleado = empleado;
	}

	/**
	 * @return the dni
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param dni the dni to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the precio
	 */
	public String getApellidos() {
		return apellidos;
	}

	/**
	 * @param precio the precio to set
	 */
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	


	/**
	 * @return the empleado
	 */
	public Departamento getEmpleado() {
		return empleado;
	}

	/**
	 * @param empleado the empleado to set
	 */
	public void setEmpleado(Departamento empleado) {
		this.empleado = empleado;
	}

	//Imprimir datos por consola, se puede quitar
	@Override
	public String toString() {
		return "Empleado [dni=" + id + ", nombre=" + nombre + ", apellidos=" + apellidos + ", departamento=" + empleado + "]";
	}
	
}
