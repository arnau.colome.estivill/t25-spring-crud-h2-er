package com.crud.h2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T25Ej02Application {

	public static void main(String[] args) {
		SpringApplication.run(T25Ej02Application.class, args);
	}

}
