DROP TABLE IF EXISTS `empleados`;
DROP table IF EXISTS `departamento`;

CREATE TABLE `departamento` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `presupuesto` int DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `empleados` (
  `dni` varchar(10) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `id_departamento` int DEFAULT NULL,
  PRIMARY KEY (`dni`),
  FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id`)
);


insert into departamento (nombre, presupuesto) values ('Redes', '2500'), ('Historia', '1532'), ('Matematicas', '3886'), ('Sociales', '884'), ('Fisica', '1600');

insert into empleados (dni, nombre, apellidos, id_departamento) values ('39913457', 'Paco', 'Merte', 1), ('19534287', 'Alba', 'Soria', 2), ('18264388', 'Daniel', 'Mendoza', 3), ('46182495', 'Jose', 'Nieto', 4), ('46712253', 'Fernando', 'Loco', 5), ('46411552', 'Salvador', 'Juroca', 1), ('19233567', 'Pedro', 'Juan', 3);


